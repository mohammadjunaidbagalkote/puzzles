﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Puzzle3A4
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] arr = { { 1,0,0,0,0},
                            {0,0,1,1,0},
                            {0,1,1,0,0},
                            {0,0,0,0,0},
                            {1,1,0,0,1},
                            {1,1,0,0,0}};
            int ct = 0;
            int rows = 6, col = 5;
            for(int i=0;i<rows;i++)
            {
                for(int j=0;j<col-1;j++)
                {
                    if (arr[i, j] == 1 && arr[i, j + 1] == 1)
                    {
                        ct = ct + 1;
                        Console.WriteLine(i+" "+j);
                    }
                }
            }
            Console.WriteLine(ct);
        }
    }
}
