﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Puzzle3A5
{
   public class Permutation
    {
        public static void printArray(int[] a)
        {
            Console.WriteLine();
            for(int i=0;i<a.Length;i++)
            {
              //  Console.WriteLine();
                Console.Write(a[i]+" ");
               // Console.WriteLine();
            }
        }
        public static void swap(int[] a,int i,int j)
        {
            int temp = a[i];
            a[i] = a[j];
            a[j] = temp;
        }
        public static void printPermutation(int[] a, int cid)
        {
            if (cid == a.Length - 1)
            {
                printArray(a);
                return;
            }
            for (int i = cid; i < a.Length; i++)
            {
                swap(a, i, cid);
                printPermutation(a, cid + 1);
                swap(a, i, cid);
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = { 1, 2, 3 };
           Permutation.printPermutation(a, 0);
        }
    }

}
