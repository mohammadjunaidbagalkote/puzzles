﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace day4_puzzle2
{
	class SparseMatrix
	{



		internal IList<int> L = new List<int>();

		// initialize with the original large array and size
		public virtual void init(int[] arr, int size)
		{
			for (int i = 0; i < size; i++)
			{
				if (arr[i] != 0)
				{
					L[i] = arr[i];
				}
			}
		}

		//updates index at i with val
		public virtual void set(int i, int val)
		{
			L[i] = val;
		}

		//gets the value at index i
		public virtual int get(int i)
		{
			return L[i];
		}

		public static void Main(string[] args)
		{
			SparseMatrix SM = new SparseMatrix();
			int[] a = new int[] { 0, 0, 0, 12, 7, 0, 13, 0, 5, 0, 27, 0, 0 };
			for (int i = 0; i < a.Length; i++)
			{
				SM.L.Add(0);
			}
			SM.init(a, a.Length);

			Console.WriteLine(SM.get(8));
			Console.WriteLine(SM.get(2));

		}
	}

}