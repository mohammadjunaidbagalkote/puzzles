﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Puzzle4A3
{
    public class Node
    {
      public  int data;
       public Node left= null;
       public Node right = null;
       public Node(int data)
        {
            this.data = data;
        }
    }
    class Program
    {
        public static bool printPath(Node root,int sum)
        {
            if (sum == 0 && root == null)
                return true;
            if(root==null)
            {
                return false;
            }
            bool left = printPath(root.left, sum - root.data);
            bool right = false;
            if (!left)
                right = printPath(root.right, sum - root.data);
            if(left||right)
                Console.WriteLine(root.data);
            return left || right;
        }
        public static int getroottoleafsum(Node root)
        {
            if (root == null)
                return int.MinValue;
            if (root.left == null && root.right == null)
                return root.data;
            int left = getroottoleafsum(root.left);
            int right = getroottoleafsum(root.right);

            return (left > right ? left : right) + root.data;
        }
        public static void findMaxsumPath(Node root)
        {
            int sum = getroottoleafsum(root);
            Console.WriteLine(sum);

        }
        static void Main(string[] args)
        {
            Node root = new Node(10);
            root.left = new Node(5);
            root.right = new Node(5);
            root.left.left = new Node(2);
            root.right.right = new Node(1);
            root.right.right.left = new Node(-1);
            findMaxsumPath(root);
        }
    }
}
