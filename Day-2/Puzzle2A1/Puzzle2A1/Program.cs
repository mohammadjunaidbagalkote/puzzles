﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Puzzle2A1
{
    class Program
    {
        public static bool solve(char[,]a,string targetw)
        {
            int k = targetw.Length;
            int row = a.GetLength(0);
            int col = a.GetLength(1);
            bool status = false;
            for(int i=0;i<(row-(k-1)); i++)
            {
                for(int j=0;j<(col-(k-1));j++)
                {
                    if (a[i,j] == targetw[0] && a[i,j + 1] == targetw[1] && a[i,j + 2] == targetw[2] && a[i,j + 3] == targetw[3])
                        return true;
                    if (a[i,j] == targetw[0] && a[i + 1,j] == targetw[1] && a[i + 2,j] == targetw[2] && a[i + 3,j] == targetw[3])
                        return true;

                }
            }
            return status;

        }
        static void Main(string[] args)
        {
            string targetw = "MASS";
            char[,] a = { { 'F', 'A', 'O', 'I' }, { 'O', 'B', 'Q', 'P' }, { 'A', 'N', 'O', 'B' }, { 'M', 'A', 'S', 'S' } };
            
    
            Console.WriteLine(solve(a,targetw));
        }
    }
}
