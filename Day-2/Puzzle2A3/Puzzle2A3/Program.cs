﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Puzzle2A3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = { -10, -10, 5, 2 };
            int k = 3, n = 0, product = 1;
            int max = int.MinValue;
            for (int i = 0; i < a.Length; i++)
            {
                int j = i;
                if (n < k)
                {
                    product = product * a[j];
                    j++;
                    n++;
                }
                if (product > max)
                    max = product;
            }
            Console.WriteLine(max);
        }
    }
}
