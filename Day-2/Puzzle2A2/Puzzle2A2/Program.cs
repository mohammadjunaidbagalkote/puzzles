﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Puzzle2A2
{
    class Program
    {
        public static bool EqualSumPartition(int[] arr, int n, int sum)
        {
            if (sum == 0)
                return true;
            if (n == 0 && sum != 0)
                return false;
            if (arr[n - 1] > sum)
                return EqualSumPartition(arr, n - 1, sum);


            return EqualSumPartition(arr, n - 1, sum) || EqualSumPartition(arr, n - 1, sum - arr[n - 1]);

        }
        public static bool findPartition(int[] arr, int n)
        {
            int sum = 0;
            for (int i = 0; i < n; i++)
            {
                sum = sum + arr[i];
            }
            if (sum % 2 != 0)
                return false;

            return EqualSumPartition(arr, n, sum / 2);
        }
        static void Main(string[] args)
        {
           
            int[] arr = { 15, 5, 20, 10, 35, 15, 10 };
            int n = arr.Length;
            if(findPartition(arr,n)==true)
                Console.WriteLine("can be divided into two subsets of equal sum");
            else
                Console.WriteLine("cannot be divided");


        }
    }
}
