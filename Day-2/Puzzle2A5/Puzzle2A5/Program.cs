﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Puzzle2A5
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = { 9, 11, 3,8, 5, 7, 10};
            int max = int.MinValue;
            int buy = 0;
            for(int i=0;i<a.Length;i++)
            {
                buy = a[i];
                for(int j=i+1;j<a.Length;j++)
                {
                    if (a[j]-buy > max)
                        max = a[j]-buy;
                }
            }
            Console.WriteLine(max);
        }
    }
}
