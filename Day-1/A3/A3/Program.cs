﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A3
{
    class Program
    {
       
            static int minHalls(int[] start,int[] end)             {
                Array.Sort(start);
                Array.Sort(end);
                int l = start.Length;
                int i = 1;
                int j = 0;
                int minMeetingsrequired = 1;
                int noOfOngoingMeetingrequired = 1;
                while (i < l && j < l)
                {
                    if (end[j] > start[i])
                    {
                        noOfOngoingMeetingrequired++;
                        if (minMeetingsrequired < noOfOngoingMeetingrequired)
                        {
                            minMeetingsrequired = noOfOngoingMeetingrequired;
                        }
                        i++;
                    }
                    else
                    {
                        noOfOngoingMeetingrequired--;
                        j++;
                    }
                }
                return minMeetingsrequired;
            }

 static void Main(string[] args)
{
    int[] start = { 30, 0, 60 };
    int[] end = { 75, 50, 150 };
    Console.WriteLine(minHalls(start,end));
}
              
    }
}   