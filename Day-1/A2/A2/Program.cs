﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A2
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList<int> list1 = new LinkedList<int>();
            list1.AddLast(1);
            list1.AddLast(2);
            list1.AddLast(3);
            LinkedList<int> list2 = new LinkedList<int>();
            list2.AddLast(4);
            list2.AddLast(1);
            list2.AddLast(6);
            foreach (var i in list1)
            {
                foreach (var j in list2)
                {
                    if (i == j)
                    {
                        Console.WriteLine("intersecting elements are " + i);
                    }

                }
            }

        }
    }
}
