﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A5
{
    class Program
    {
        static int[] arr = new int[5];
        static int index = 0;
        static int a;
        static void Main(string[] args)
        {
            print();
            push(5);
            push(10);
            push(15);
            push(20);
            print();
            int a = pop();
            Console.WriteLine();
            Console.WriteLine(a);
            print();
            Console.WriteLine();
            max();
        }
        public static void push(int value)
        {
            if (index < arr.Length)
            {
                arr[index++] = value;
            }
            else
                Console.WriteLine("stack overflow");
        }
        public static int pop()
        {
            if (index > 0)
            {
                int a = arr[--index];
                arr[index] = 0;
                return a;
            }
            else
            {
                Console.WriteLine("stack is empty");
                return 0;
            }
        }
        public static void print()
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + " ");
            }
        }
        public static void max()
        {
            if (index > 0)
            {
                Array.Sort(arr);
                Console.WriteLine(arr[arr.Length - 1]);
            }
            else
                Console.WriteLine("array contains zero elements");


        }
    }
}
